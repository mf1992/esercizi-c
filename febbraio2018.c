#include <stdio.h>
#include <stdlib.h>

typedef struct anagrafe{
char cognome[20];
char nome [20];
float media;
}anagrafe;

anagrafe * leggi_file(FILE*input, int *dim_vect);
void calcola(anagrafe *vect,float *tot_media, int dim_vect);
void scrivi_file(FILE *output, anagrafe *vect_anagrafe, anagrafe *max, int dim_vect);


int main(int argc, char *argv[])
{

anagrafe *vect,*max;
char domanda[]="Quanto e' la dimensione";
float tot_media;
int *dim_vect;
int z=0;
dim_vect=&z;
FILE *input, *output;
if((input=fopen(argv[1],"r"))==NULL){
	
	printf("Errore di apertura di %s\n", argv[1]);
}
if((output=fopen(argv[2],"w"))==NULL)
{
	printf("Errore di apertura di %s\n", argv[2]);
}
puts(domanda);

scanf("%d", dim_vect);

vect=leggi_file(input, dim_vect);

fclose(input);
calcola(vect, &tot_media, *dim_vect);
	
scrivi_file(output,vect,max,*dim_vect);
fclose(output);
return 0;
}
/*1. anagrafe *leggi_file(FILE *input, int *dim_vect): crea un vettore di tipo anagrafe a partire dal file input (il cui nome viene *passato sulla linea di comando con l’eseguibile). Il file input contiene una sequenza (non prefissata) di stringhe (cognomi e nomi *di studenti) e per ogni coppia la relativa media degli esami sostenuti (Esempio: Verdi Paola 27.5 Rossi Mario 22.3 Bianchi Luisa *30.0 Rocchi Antonio 26.6…..): ognuna delle triple (cognome, nome, media) sarà copiata nella posizione i-esima del vettore creato *dinamicamente il cui puntatore sarà restituito al main. In dim_vect viene merorizzata la dimensione del vettore. (Punti: 10)
*/
anagrafe *leggi_file(FILE*input, int *dim_vect)
{
anagrafe *vect=malloc(*dim_vect * (sizeof(anagrafe)));

while(fgetc(input)!=EOF){
	for(int i=0;i<*dim_vect;i++){
    fscanf(input,"%s %s %f",vect[i].cognome,vect[i].nome,&vect[i].media);	
}
}
return vect;
}
/*
*2. void calcola(anagrafe *vect, float * tot_media, int dim_vect): calcola la media degli studenti presenti nel vettore e la memorizza in tot_media, mentre dim_vect rappresenta la dimensione di vect. (Punti: 8)
*/
void calcola(anagrafe *vect,float *tot_media, int dim_vect)
{
	for(int i=0;i<dim_vect;i++)
	{
		*tot_media+=vect[i].media;
	}
	*tot_media=*tot_media/dim_vect;
}
/*3. void scrivi_file(FILE *output, anagrafe *vect, anagrafe **max, int dim_vect): stampa nel file output (il cui nome viene passato *sulla linea di comando con l’eseguibile) il record dello studente con la media più alta e ne memorizza l’indirizzo (del record) in *max, mentre dim_vet rappresenta la dimensione di vect. (Punti: 12)
*/
void scrivi_file(FILE *output, anagrafe *vect_anagrafe, anagrafe *max, int dim_vect)
{
	max=malloc(sizeof(anagrafe));
	max=vect_anagrafe;
	for(int i=0;i<dim_vect;i++)
	{
		if(vect_anagrafe[i].media >= max->media)
		{
			*max=vect_anagrafe[i];
		}
		
	}
fprintf(output,"%s\t%s\t%f\t",max->nome, max->cognome,max->media);

}

